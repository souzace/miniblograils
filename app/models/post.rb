class Post < ActiveRecord::Base
	validates :author, presence: true
	validates :description, presence: true
	validates_length_of :description, :maximum => 500
end