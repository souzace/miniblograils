# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$(document).ready ->

  $(document).bind "ajaxSuccess", "form.post_form", (event, xhr, settings) ->
    $post_form = $(event.data)
    description = xhr.responseJSON.description 
    author = xhr.responseJSON.author
    if xhr.responseJSON.id > 0 
    	$("#listPosts").prepend("<p>"+description + "<br/>" + author + "</p>").hide().fadeIn(1000)
    
    $error_container = $("#error_explanation", $post_form)
    $error_container_ul = $("ul", $error_container)
    if $("li", $error_container_ul).length
      $("li", $error_container_ul).remove()
      $error_container.hide()

  $(document).bind "ajaxError", "form.post_form", (event, jqxhr, settings, exception) ->
    $post_form = $(event.data)
    $error_container = $("#error_explanation", $post_form)
    $error_container_ul = $("ul", $error_container)
    $error_container.show()  if $error_container.is(":hidden")
    $.each jqxhr.responseJSON, (index, message) ->
      $("<li>").html(message).appendTo $error_container_ul